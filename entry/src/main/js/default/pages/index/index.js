import router from '@system.router'

var pk1Val = null;
var pk2Val = null;

export default {
    data: {
        pk1range : ["1","2","3"],
        pk2range : ["较快","舒缓","较慢"]
    },
    onInit(){
        console.log('主界面-onInit');
    },
    onReady(){
        console.log('主界面-onReady');
    },
    onShow(){
        console.log('主界面-onShow');
    },
    onDestroy(){
        console.log('主界面-onDestroy');
    },
    clickAction(){
        router.replace({
            uri : 'pages/daojishi/daojishi',
            params : {
                "pk1Val":pk1Val,
                "pk2Val":pk2Val
            }
        });
    },
    choosePk1(pk1){
        var val = pk1.newValue;
        console.log('pk1:'+val);
        pk1Val = val;
    },
    choosePk2(pk2){
        var val = pk2.newValue;
        console.log('pk2:'+val);
        pk2Val = val;
    }

}
