import router from '@system.router'


var pk1Val = null;
var pk2Val = null;

var timer = null;
var counter = 3;
export default {
    data: {
        time:3,
        imgsrc:''
    },
    run() {
        counter-=1;
        if (counter != 0) {
            this.imgsrc = "/common/" + counter.toString() + ".png";
            this.time = counter.toString();
        } else {
            this.imgsrc = "";
            this.time = "";

            clearInterval(timer);
            timer = null;

            router.replace({
                uri: 'pages/xunlian/xunlian',
                params: {"key1": pk1Val, "key2": pk2Val}
            });
        }
    },
    onInit(){
        var that = this;
        pk1Val = that.pk1Val;
        pk2Val = that.pk2Val;
        console.log(pk1Val);
        console.log(pk2Val);

    },
    onShow(){
        timer = setInterval(this.run, 1000);
    }
}
