import router from '@system.router'

var pk1Val = null;
var pk2Val = null;

var pk1Seconds = null;
var pk2Seconds = null;
var timer1 = null;
var timer2 = null;
var timer3 = null;

var counter = 0;

export default {
    data: {
        seconds:0,
        showText:true,
        type:"吸气",
        percent:'0',
        duration:'',
        count:''
    },
    onInit(){


        console.log('训练界面-onInit');
        //console.log('左边值'+this.key1);
        //console.log('右边值'+this.key2)
        pk1Val = this.key1;
        pk1Seconds = pk1Val * 60;
        this.seconds = pk1Seconds;
        pk2Val = this.key2;
        if(pk2Val == '较慢'){
            pk2Seconds = 6;
        }else if(pk2Val == '舒缓'){
            pk2Seconds = 4;
        }else if(pk2Val == '较快'){
            pk2Seconds = 2;
        }
        this.duration = pk2Seconds+'s';
        this.count = (pk1Seconds/pk2Seconds).toString();
    },
    run1(){
        this.seconds-=1;
        if(this.seconds==0){
            clearInterval(timer1);
            timer1=null;
            this.showText = false;
        }
    },
    run2(){
        //console.log('run2');
        counter++;
        if(counter == pk1Seconds / pk2Seconds){
            clearInterval(timer2);
            timer2 = null;
            this.type = '已完成';
        }else{
            if(this.type == '呼气'){
                this.type = '吸气';
            }else if(this.type == '吸气'){
                this.type = '呼气';
            }
        }
    },
    run3() {
        //console.log('run3');
        this.percent = (parseInt(this.percent) + 1).toString();
        if (parseInt(this.percent) < 10) {
            this.percent = "0" + this.percent;
        }
        if (parseInt(this.percent) == 100) {
            this.percent = "0";
        }
        if (timer2 == null) {
            clearInterval(timer3);
            timer3 = null;
            this.percent = "100";
        }
    },
    onReady(){
        console.log('训练界面-onReady');
    },
    onShow(){
        console.log('训练界面-onShow');
        timer1 = setInterval(this.run1,1000);

        timer2 = setInterval(this.run2, pk2Seconds * 1000);

        timer3 = setInterval(this.run3, pk2Seconds / 100 * 1000);

    },
    onDestroy(){
        console.log('训练界面-onDestroy');
    },
    clickAction(){
        clearInterval(timer1);
        timer1 =null;

        clearInterval(timer2);
        timer2 =null;

        clearInterval(timer3);
        timer3 =null;

        router.replace({
            uri : 'pages/index/index'
        });
    }
}
